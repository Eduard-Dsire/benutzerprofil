<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Eduard.ExtProfil',
            'Profil',
            [
                'Benutzerprofil' => 'list, show, new, create, edit, update, delete, login, export, forgot'
            ],
            // non-cacheable actions
            [
                'Benutzerprofil' => 'create, update, delete, login, forgot'
            ]
        );

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins {
                    elements {
                        profil {
                            iconIdentifier = ext_profil-plugin-profil
                            title = LLL:EXT:ext_profil/Resources/Private/Language/locallang_db.xlf:tx_ext_profil_profil.name
                            description = LLL:EXT:ext_profil/Resources/Private/Language/locallang_db.xlf:tx_ext_profil_profil.description
                            tt_content_defValues {
                                CType = list
                                list_type = extprofil_profil
                            }
                        }
                    }
                    show = *
                }
           }'
        );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);

			$iconRegistry->registerIcon(
				'ext_profil-plugin-profil',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:ext_profil/Resources/Public/Icons/user_plugin_profil.svg']
			);

    }
);
