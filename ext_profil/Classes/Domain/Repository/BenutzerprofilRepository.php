<?php
namespace Eduard\ExtProfil\Domain\Repository;


/***
 *
 * This file is part of the "User Profil" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2021 
 *
 ***/
/**
 * The repository for Benutzerprofils
 */
class BenutzerprofilRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
}
