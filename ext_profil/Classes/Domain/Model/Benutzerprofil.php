<?php
namespace Eduard\ExtProfil\Domain\Model;


/***
 *
 * This file is part of the "User Profil" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2021 
 *
 ***/
/**
 * Benutzerprofil
 */
class Benutzerprofil extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * name
     * 
     * @var string
     */
    protected $name = '';

    /**
     * firstname
     * 
     * @var string
     */
    protected $firstname = '';

    /**
     * email
     * 
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $email = '';

    /**
     * profilImage
     * 
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $profilImage = null;

    /**
     * password
     * 
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $password = '';

    /**
     * passwordCheck
     * 
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $passwordCheck = '';

    /**
     * birthDate
     * 
     * @var string
     */
    protected $birthDate = '';

    /**
     * Returns the name
     * 
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name
     * 
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Returns the firstname
     * 
     * @return string $firstname
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Sets the firstname
     * 
     * @param string $firstname
     * @return void
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * Returns the email
     * 
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the email
     * 
     * @param string $email
     * @return void
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Returns the profilImage
     * 
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $profilImage
     */
    public function getProfilImage()
    {
        return $this->profilImage;
    }

    /**
     * Sets the profilImage
     * 
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $profilImage
     * @return void
     */
    public function setProfilImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $profilImage)
    {
        $this->profilImage = $profilImage;
    }

    /**
     * Returns the password
     * 
     * @return string $password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Sets the password
     * 
     * @param string $password
     * @return void
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Returns the passwordCheck
     * 
     * @return string $passwordCheck
     */
    public function getPasswordCheck()
    {
        return $this->passwordCheck;
    }

    /**
     * Sets the passwordCheck
     * 
     * @param string $passwordCheck
     * @return void
     */
    public function setPasswordCheck($passwordCheck)
    {
        $this->passwordCheck = $passwordCheck;
    }

    /**
     * Returns the birthDate
     * 
     * @return string $birthDate
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * Sets the birthDate
     * 
     * @param string $birthDate
     * @return void
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;
    }
}
