<?php
namespace Eduard\ExtProfil\Controller;


/***
 *
 * This file is part of the "User Profil" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2021
 *
 ***/
/**
 * BenutzerprofilController
 */
class BenutzerprofilController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * benutzerprofilRepository
     *
     * @var \Eduard\ExtProfil\Domain\Repository\BenutzerprofilRepository
     */
    protected $benutzerprofilRepository = null;

    /**
     * @param \Eduard\ExtProfil\Domain\Repository\BenutzerprofilRepository $benutzerprofilRepository
     */
    public function injectBenutzerprofilRepository(\Eduard\ExtProfil\Domain\Repository\BenutzerprofilRepository $benutzerprofilRepository)
    {
        $this->benutzerprofilRepository = $benutzerprofilRepository;
    }

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $benutzerprofils = $this->benutzerprofilRepository->findAll();
        $this->view->assign('benutzerprofils', $benutzerprofils);
    }

    /**
     * action show
     *
     * @param \Eduard\ExtProfil\Domain\Model\Benutzerprofil $benutzerprofil
     * @return void
     */
    public function showAction(\Eduard\ExtProfil\Domain\Model\Benutzerprofil $benutzerprofil)
    {
        $this->view->assign('benutzerprofil', $benutzerprofil);
    }

    /**
     * action new
     *
     * @return void
     */
    public function newAction()
    {
    }

    /**
     * action login
     *
     * @return void
     */
    public function loginAction()
    {
        $email = $_POST["tx_extprofil_profil"]["loginBenutzerprofil"]["email"];
        $password = $_POST["tx_extprofil_profil"]["loginBenutzerprofil"]["password"];
        $benutzerprofils = $this->benutzerprofilRepository->findByEmail($email);
        $user = $benutzerprofils[0];
        //debug($user);exit();
        if ($user) {
            $userPass = $user->getPassword();
            if ($password == $userPass) {
                $this->forward('show', null, null, ['benutzerprofil' => $user]);
            } else {
                $this->addFlashMessage('E-mail oder Password ist falsch!', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
            }
        } else {
            $this->addFlashMessage('Registrieren Sie sich bitte!', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        }
        $this->redirect('list');
    }

    /**
     * action forgot
     *
     * @return void
     */
    public function forgotAction()
    {

    }

    /**
     * action create
     *
     * @param \Eduard\ExtProfil\Domain\Model\Benutzerprofil $newBenutzerprofil
     * @return void
     */
    public function createAction(\Eduard\ExtProfil\Domain\Model\Benutzerprofil $newBenutzerprofil)
    {
        $this->addFlashMessage('The object was created. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->benutzerprofilRepository->add($newBenutzerprofil);
        $this->redirect('list');
    }

    /**
     * action edit
     *
     * @param \Eduard\ExtProfil\Domain\Model\Benutzerprofil $benutzerprofil
     * @ignorevalidation $benutzerprofil
     * @return void
     */
    public function editAction(\Eduard\ExtProfil\Domain\Model\Benutzerprofil $benutzerprofil)
    {
        $this->view->assign('benutzerprofil', $benutzerprofil);
    }

    /**
     * action update
     *
     * @param \Eduard\ExtProfil\Domain\Model\Benutzerprofil $benutzerprofil
     * @return void
     */
    public function updateAction(\Eduard\ExtProfil\Domain\Model\Benutzerprofil $benutzerprofil)
    {
        $this->addFlashMessage('The object was updated. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->benutzerprofilRepository->update($benutzerprofil);
        $this->redirect('list');
    }

    /**
     * action delete
     *
     * @param \Eduard\ExtProfil\Domain\Model\Benutzerprofil $benutzerprofil
     * @return void
     */
    public function deleteAction(\Eduard\ExtProfil\Domain\Model\Benutzerprofil $benutzerprofil)
    {
        $this->addFlashMessage('The object was deleted. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->benutzerprofilRepository->remove($benutzerprofil);
        $this->redirect('list');
    }
}
