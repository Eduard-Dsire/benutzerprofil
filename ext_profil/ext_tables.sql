#
# Table structure for table 'tx_extprofil_domain_model_benutzerprofil'
#
CREATE TABLE tx_extprofil_domain_model_benutzerprofil (

	name varchar(255) DEFAULT '' NOT NULL,
	firstname varchar(255) DEFAULT '' NOT NULL,
	email varchar(255) DEFAULT '' NOT NULL,
	profil_image int(11) unsigned NOT NULL default '0',
	password varchar(255) DEFAULT '' NOT NULL,
	password_check varchar(255) DEFAULT '' NOT NULL,
	birth_date varchar(255) DEFAULT '' NOT NULL

);
