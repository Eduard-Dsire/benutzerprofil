<?php
namespace Eduard\ExtProfil\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class BenutzerprofilTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \Eduard\ExtProfil\Domain\Model\Benutzerprofil
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Eduard\ExtProfil\Domain\Model\Benutzerprofil();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getNameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getName()
        );
    }

    /**
     * @test
     */
    public function setNameForStringSetsName()
    {
        $this->subject->setName('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'name',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFirstnameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getFirstname()
        );
    }

    /**
     * @test
     */
    public function setFirstnameForStringSetsFirstname()
    {
        $this->subject->setFirstname('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'firstname',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getEmailReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getEmail()
        );
    }

    /**
     * @test
     */
    public function setEmailForStringSetsEmail()
    {
        $this->subject->setEmail('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'email',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getProfilImageReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getProfilImage()
        );
    }

    /**
     * @test
     */
    public function setProfilImageForFileReferenceSetsProfilImage()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setProfilImage($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'profilImage',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getPasswordReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getPassword()
        );
    }

    /**
     * @test
     */
    public function setPasswordForStringSetsPassword()
    {
        $this->subject->setPassword('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'password',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getPasswordCheckReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getPasswordCheck()
        );
    }

    /**
     * @test
     */
    public function setPasswordCheckForStringSetsPasswordCheck()
    {
        $this->subject->setPasswordCheck('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'passwordCheck',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getBirthDateReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getBirthDate()
        );
    }

    /**
     * @test
     */
    public function setBirthDateForStringSetsBirthDate()
    {
        $this->subject->setBirthDate('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'birthDate',
            $this->subject
        );
    }
}
