<?php
namespace Eduard\ExtProfil\Tests\Unit\Controller;

/**
 * Test case.
 */
class BenutzerprofilControllerTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \Eduard\ExtProfil\Controller\BenutzerprofilController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Eduard\ExtProfil\Controller\BenutzerprofilController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllBenutzerprofilsFromRepositoryAndAssignsThemToView()
    {

        $allBenutzerprofils = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $benutzerprofilRepository = $this->getMockBuilder(\Eduard\ExtProfil\Domain\Repository\BenutzerprofilRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $benutzerprofilRepository->expects(self::once())->method('findAll')->will(self::returnValue($allBenutzerprofils));
        $this->inject($this->subject, 'benutzerprofilRepository', $benutzerprofilRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('benutzerprofils', $allBenutzerprofils);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenBenutzerprofilToView()
    {
        $benutzerprofil = new \Eduard\ExtProfil\Domain\Model\Benutzerprofil();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('benutzerprofil', $benutzerprofil);

        $this->subject->showAction($benutzerprofil);
    }

    /**
     * @test
     */
    public function createActionAddsTheGivenBenutzerprofilToBenutzerprofilRepository()
    {
        $benutzerprofil = new \Eduard\ExtProfil\Domain\Model\Benutzerprofil();

        $benutzerprofilRepository = $this->getMockBuilder(\Eduard\ExtProfil\Domain\Repository\BenutzerprofilRepository::class)
            ->setMethods(['add'])
            ->disableOriginalConstructor()
            ->getMock();

        $benutzerprofilRepository->expects(self::once())->method('add')->with($benutzerprofil);
        $this->inject($this->subject, 'benutzerprofilRepository', $benutzerprofilRepository);

        $this->subject->createAction($benutzerprofil);
    }

    /**
     * @test
     */
    public function editActionAssignsTheGivenBenutzerprofilToView()
    {
        $benutzerprofil = new \Eduard\ExtProfil\Domain\Model\Benutzerprofil();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('benutzerprofil', $benutzerprofil);

        $this->subject->editAction($benutzerprofil);
    }

    /**
     * @test
     */
    public function updateActionUpdatesTheGivenBenutzerprofilInBenutzerprofilRepository()
    {
        $benutzerprofil = new \Eduard\ExtProfil\Domain\Model\Benutzerprofil();

        $benutzerprofilRepository = $this->getMockBuilder(\Eduard\ExtProfil\Domain\Repository\BenutzerprofilRepository::class)
            ->setMethods(['update'])
            ->disableOriginalConstructor()
            ->getMock();

        $benutzerprofilRepository->expects(self::once())->method('update')->with($benutzerprofil);
        $this->inject($this->subject, 'benutzerprofilRepository', $benutzerprofilRepository);

        $this->subject->updateAction($benutzerprofil);
    }

    /**
     * @test
     */
    public function deleteActionRemovesTheGivenBenutzerprofilFromBenutzerprofilRepository()
    {
        $benutzerprofil = new \Eduard\ExtProfil\Domain\Model\Benutzerprofil();

        $benutzerprofilRepository = $this->getMockBuilder(\Eduard\ExtProfil\Domain\Repository\BenutzerprofilRepository::class)
            ->setMethods(['remove'])
            ->disableOriginalConstructor()
            ->getMock();

        $benutzerprofilRepository->expects(self::once())->method('remove')->with($benutzerprofil);
        $this->inject($this->subject, 'benutzerprofilRepository', $benutzerprofilRepository);

        $this->subject->deleteAction($benutzerprofil);
    }
}
