<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Eduard.ExtProfil',
            'Profil',
            'Profil'
        );

        if (TYPO3_MODE === 'BE') {

            \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
                'Eduard.ExtProfil',
                'web', // Make module a submodule of 'web'
                'backuserprofil', // Submodule key
                '', // Position
                [
                    'Benutzerprofil' => 'list, show, new, create, edit, update, delete',
                ],
                [
                    'access' => 'user,group',
                    'icon'   => 'EXT:ext_profil/Resources/Public/Icons/user_mod_backuserprofil.svg',
                    'labels' => 'LLL:EXT:ext_profil/Resources/Private/Language/locallang_backuserprofil.xlf',
                ]
            );

        }

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('ext_profil', 'Configuration/TypoScript', 'User Profil');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_extprofil_domain_model_benutzerprofil', 'EXT:ext_profil/Resources/Private/Language/locallang_csh_tx_extprofil_domain_model_benutzerprofil.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_extprofil_domain_model_benutzerprofil');

    }
);
